var firebase = require("firebase");

var fs = require('fs');
var obj;
//database config for the firebase app
var config = {
    apiKey: "AIzaSyDZghp_J6OEJJuvUMx5KVGDHQIEEIlBqX4",
    authDomain: "mobgrazer-dev.firebaseapp.com",
    databaseURL: "https://mobgrazer-dev.firebaseio.com",
    storageBucket: "mobgrazer-dev.appspot.com",
    messagingSenderId: "196180234733"
};
firebase.initializeApp(config);

//ParseAnimalPen.json is the exported file from Parse to be converted to AnimalPen object and pushed to firebase
fs.readFile('ParseAnimalPen.json', 'utf8', function (err, data) {
    if (err) throw err;
    obj = JSON.parse(data);
   console.log(obj.results.length)
    var animalPenRef =  firebase.database().ref('AnimalPen');
    animalPenRef.remove();
    for (var i = 0; i < obj.results.length; i++) {
        var pen = obj.results[i];

        var animalPen = new Object();
       animalPen.averageWeight = pen.average_weight;
       animalPen.brixScore = pen.brix_score;
       animalPen.campName = pen.camp_name;
       animalPen.cattleCount = pen.cattle_count;
       animalPen.timeIn = new Date(pen.time_in.iso).getTime();
       animalPen.timeOut = new Date(pen.time_out.iso).getTime();
       animalPen.centerPoint = getCentrePoint(pen);
       animalPen.penPoints = getPenPoints(pen);
       animalPen.cattleType = getCattleType(pen);

        animalPenRef.push().set(animalPen);
        console.log("Animal pen saved successfully : "+animalPen.campName)
    }

    console.log("done saving data...")

});

function getPenPoints(animalpen){
    var points = [];
    if(animalpen.pen_points){
        for (var i = 0; i < animalpen.pen_points.length; i++) {
            var p = animalpen.pen_points[i];
            points.push({
                latitude: p.latitude,
                longitude: p.longitude
            })
        }
    }
    return points;
}


function getCentrePoint(animalpen){
    var point = {};
    if(animalpen.center_point){
      point.latitude = animalpen.center_point.latitude;
      point.longitude = animalpen.center_point.longitude;
    }
    return point;
}

function getCattleType(animalpen){
    var types = [];
    if(animalpen.cattle_type){
        for (var i = 0; i < animalpen.cattle_type.length; i++) {
           types.push(animalpen.cattle_type[i])
        }
    }
    return types;
}