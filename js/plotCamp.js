/**
 * Created by Ryno Claassen on 15/02/15.
 */
var AnimalPen;
var query;
var objectID;

var dateStart;
var dateEnd;

$(document).ready(function() {

    $('#datepickerStart').datepicker({
        format: "dd M yyyy"
    })
        .on('changeDate', function(ev){
            dateStart = $(this).datepicker('getDate');
            plot(dateStart, dateEnd);
            $('#updateCamp').hide();
            clearFields();
        });

    $('#datepickerEnd').datepicker({
        format: "dd M yyyy"
    })
        .on('changeDate', function(ev){
            dateEnd = $(this).datepicker('getDate');
            plot(dateStart, dateEnd);
            $('#updateCamp').hide();
            clearFields();
        });

    $('#updateCamp').click(function() {
        updateCamp(objectID, $('#cattle_count').val(), $('#brix_score').val(), $('#average_weight').val(), $('#camp_name').val());
    });
});

function init() {
    dateStart = new Date();
    dateEnd = new Date();
    $('#datepickerStart').val("Today");
    $('#datepickerEnd').val("Today");
    plot(dateStart, dateEnd);
}

//This is the new method to plot camps from Firebase data
function plot(dateStart, dateEnd) {
    var results = [];
    var startDate =  new Date(dateStart.setHours(0, 0, 0, 0));
    var endDate = new Date(dateEnd.setHours(23, 59, 59, 999));

    //This call will only get new data that has been created by the app into firebase - Parse data cannot be read here  :-(
    firebase.database().ref('AnimalPen').orderByChild("timeIn").startAt(startDate.getTime()).endAt(endDate.getTime()).once('value', function (dataSnapshot) {
        dataSnapshot.forEach(function(child) {
            results.push(child.val())
        });
        if(results.length == 0){
            $('#googleMap').hide();
            $('#campText').text("No camps were created on this day");
        }else{
            $('#googleMap').show();
            drawCamps(results);
        }
    }, function (err) {
        // code to handle read error
        console.log(err)
    });

}

function clearFields() {
    $('#time_in').val("");
    $('#time_out').val("");
    $('#duration').val("");
    $('#cattle_type').val("");
    $('#cattle_count').val("");
    $('#brix_score').val("");
    $('#average_weight').val("");
    $('#campText').text("");
}

function drawCamps(results) {

    var camps = [];

    var mapOptions = {
        zoom: 20,
        mapTypeId: google.maps.MapTypeId.SATELLITE,
        mapTypeControl: false,
        streetViewControl: false,
        panControl: false,
        zoomControlOptions: {
            position: google.maps.ControlPosition.RIGHT_TOP
        }
    };

    var map = new google.maps.Map(document.getElementById('googleMap'),
        mapOptions);

    var bounds = new google.maps.LatLngBounds();

    for (var resCnt = 0; resCnt < results.length; resCnt++) {
        var query = results[resCnt];
        var campCords = [];
        for (var campCnt = 0; campCnt < query.penPoints.length; campCnt++) {
            campCords.push(new google.maps.LatLng(
                query.penPoints[campCnt].latitude,
                query.penPoints[campCnt].longitude
            ));
            bounds.extend(campCords[campCords.length - 1]);
        } // End of pen_points for loop

        var camp = new google.maps.Polygon({
            paths: campCords,
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.35
        });
        var timeOutDate = new Date(results[resCnt].timeOut);
        var timeInDate = new Date(results[resCnt].timeIn);
        if(days_between(timeOutDate, new Date()) < 7) {
            camp.setOptions({strokeColor: '#FF0000', fillColor: '#FF0000'});
        } else if(days_between(timeOutDate, new Date()) >=7 && days_between(timeOutDate, new Date()) < 14) {
            camp.setOptions({strokeColor: '#FFAD5C', fillColor: '#FFAD5C'});
        } else {
            camp.setOptions({strokeColor: '#99D699', fillColor: '#99D699'});
        }

        camps.push(camp);

        google.maps.event.addListener(camp, 'click', (function (camp, resCnt) {
            return function () {
                // Reset Colors
                for (var i = 0; i < camps.length; i++) {
                    var c = camps[i].strokeColor;
                    camps[i].setOptions({fillColor: c});
                }

                objectID = results[resCnt].id;
                camp.setOptions({fillColor: '#0000FF'});
                var month = timeInDate.getMonth() + 1;
                $('#time_in').val(timeInDate.getUTCHours() + "h " + timeInDate.getMinutes() + "m | " + timeInDate.getDate() + "/" +  month + "/" +  timeInDate.getFullYear() );
                $('#time_out').val(timeOutDate.getUTCHours() + "h " + timeOutDate.getMinutes() + "m | " + timeOutDate.getDate() + "/" +  month + "/" +  timeOutDate.getFullYear() );
                var minutes = (((timeInDate.getTime() - timeInDate.getTime())/(1000*60)) % 60);
                var hours = (((timeInDate.getTime() - timeOutDate.getTime())/(1000*60*60)) % 24);
                $('#duration').val(parseInt(Math.abs(hours)) + "h "+ parseInt(Math.abs(minutes)) + "m");
                //$('#duration').val( (results[resCnt].get("time_in").getUTCHours() - results[resCnt].get("time_out").getUTCHours()) + "h " + (results[resCnt].get("time_in").getUTCMinutes() - results[resCnt].get("time_out").getUTCMinutes()) + "m")
                $('#cattle_type').val(results[resCnt].cattleType);
                $('#brix_score').val(results[resCnt].brixScore);
                $('#cattle_count').val(results[resCnt].cattleCount);
                $('#average_weight').val(results[resCnt].averageWeight);
                $('#camp_name').val(results[resCnt].campName);
                $('#updateCamp').show(); // Show update button.
            }
            }) (camp, resCnt));

            camp.setMap(map);

    } // End of results for loop

    map.fitBounds(bounds);
    google.maps.event.addDomListener(window, 'load');

} // Function draw camps

function days_between(date1, date2) {

    // The number of milliseconds in one day
    var ONE_DAY = 1000 * 60 * 60 * 24;

    // Convert both dates to milliseconds
    var date1_ms = date1.getTime();
    var date2_ms = date2.getTime();

    // Calculate the difference in milliseconds
    var difference_ms = Math.abs(date1_ms - date2_ms);

    // Convert back to days and return
    return Math.round(difference_ms/ONE_DAY);

}





