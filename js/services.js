function submitFeedback() {

	var Feedback = Parse.Object.extend("Feedback");
	var feedback = new Feedback();

	feedback.set("feedback", $('#feedback').val());

	feedback.save(null, {
		success: function(feedback) {
			alert("Thank you for your feedback!");
		},
		error: function(feedback, error) {
			alert("Could not submit your feedback at this time. Try again later.\nError: " + error.message);
		}
	});

	$('#feedback').val("");
}

function populateFeedbackTable() {

    var Feedback = Parse.Object.extend("Feedback");
    var feedback = new Parse.Query(Feedback);

    feedback.find({
        success: function (results) {

            $('#thetable tr').not(':first').not(':last').remove();
            var html = '';
            for(var i = 0; i < results.length; i++)
                html += '<tr><td>' + results[i].get("feedback") + '</td></tr>';
            $('#thetable tr').first().after(html);

        },
        error: function (object, error) {
            alert("Error");
        }
    });
}

function updateCamp(parseObjectID, cattle_count, brix_score, average_weight, camp_name) {

	var AnimalPen = Parse.Object.extend("ParseAnimalPen");
    var animalPen = new AnimalPen();

    animalPen.id = parseObjectID;
	animalPen.set("cattle_count", cattle_count + ""); // Cast to string
	animalPen.set("brix_score", brix_score + ""); // Cast to string
    animalPen.set("average_weight", average_weight + ""); // Cast to string
    animalPen.set("camp_name", camp_name + ""); // Cast to string

    animalPen.save(null, {
    	success: function(parseObject) {
       		alert("Update successful!");
            location.reload();
    	},
    	error: function(parseObject, error) {
    		alert("Could not update the camp. Please try again later.\nError: " + error.message);
    	}
    });

}